import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FullWidthComponent } from './full-width.component';
import { LoginComponent } from 'src/app/modules/login/login.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
      FullWidthComponent,
    //   LoginComponent
    ],
    imports: [
      CommonModule,
      RouterModule,
      SharedModule,
      MatFormFieldModule,
      MatInputModule,
      MatSelectModule,
      FormsModule
    ]
  })
  export class FullwidthModule { }
