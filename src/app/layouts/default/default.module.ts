import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default.component';
import { ExploreComponent } from 'src/app/modules/explore/explore.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListsComponent } from 'src/app/modules/lists/lists.component';
import { SidebarModule } from 'ng-sidebar';
import { DetailComponent } from 'src/app/modules/detail/detail.component';
import { TweetCardComponent } from 'src/app/modules/tweet-card/tweet-card.component';
import { TweetsComponent } from 'src/app/modules/profile-page/profile/tweets/tweets.component';
import { TweetsRepliesComponent } from 'src/app/modules/profile-page/profile/tweets-replies/tweets-replies.component';
import { ProfilePageComponent } from 'src/app/modules/profile-page/profile-page.component';
import { HomeComponent } from 'src/app/modules/home-page/home/home.component';
import { AllComponent } from 'src/app/modules/notification-page/notifications/all/all.component';
import { MentionedComponent } from 'src/app/modules/notification-page/notifications/mentioned/mentioned.component';
import { BookmarksComponent } from 'src/app/modules/bookmark-page/bookmarks/bookmarks.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FollowlistComponent } from 'src/app/modules/profile-page/followlist/followlist.component';


@NgModule({
  declarations: [
    DefaultComponent,
    ExploreComponent,
    BookmarksComponent,
    ListsComponent,
    DetailComponent,
    MentionedComponent,
    AllComponent,
    TweetCardComponent,
    // ProfilePageComponent,
    TweetsComponent,
    TweetsRepliesComponent,
    HomeComponent,
    FollowlistComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    SidebarModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class DefaultModule { }
