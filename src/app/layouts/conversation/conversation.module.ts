import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConversationComponent } from './conversation.component';
import { MessagesComponent } from 'src/app/modules/messages/messages.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { SidebarModule } from 'ng-sidebar';



@NgModule({
  declarations: [
    ConversationComponent,
    MessagesComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    SidebarModule.forRoot(),
  ]
})
export class ConversationModule { }
