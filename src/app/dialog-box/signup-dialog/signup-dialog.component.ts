import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { RegisterService } from 'src/app/services/register.service';
import { AngularFireAuth } from '@angular/fire/auth';
import  auth  from 'firebase/app';


@Component({
  selector: 'app-signup-dialog',
  templateUrl: './signup-dialog.component.html',
  styleUrls: ['./signup-dialog.component.css']
})
export class SignupDialogComponent implements OnInit {

  activeName:boolean=false;
  activePhone:boolean=false;
  activeEmail:boolean=false;
  showPhone:boolean=true;
  showEmail:boolean=false;
  enable:boolean=false;
  nameValue:string='';
  phoneValue:string='';
  emailValue:string='';
  monthValue:string='';
  dayValue:string='';
  yearValue:string='';
  hide1:boolean=false;
  hide2:boolean=true;
  hide3:boolean=true;
  hide4:boolean=true;
  selectedMonth:number=0;
  selectedDay:number;
  selectedYear:number;
  date = new Date();
  months:string[]=['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                    'October', 'November', 'December'];
  days:number[]=[];
  years:number[]=[];
  enableExpirence:boolean=false;
  today = new Date();

  currentDate = this.today.getDate();
  currentMonth = this.months[this.today.getMonth()];
  defaultProfilePic = '../../../assets/default-profile-pic.png';
  defaultBannerPic = '../../../assets/bannerPic.jpg';
  
   
  currentYear = this.today.getFullYear();
  doj:string = this.currentMonth+' '+this.currentYear;
  signUpForm: FormGroup;

  @ViewChild('name') name:ElementRef
  @ViewChild('phone') phone:ElementRef
  @ViewChild('email') email:ElementRef
  @ViewChild('id') id:ElementRef
  @ViewChild('month') month:ElementRef
  @ViewChild('day') day:ElementRef
  @ViewChild('year') year:ElementRef

  constructor(public dialogRef: MatDialogRef<SignupDialogComponent>,
    public fb: FormBuilder,
    public registerService:RegisterService,
    
    ) { 
    dialogRef.disableClose = true; 
    for(let i=2021;i>=1901;i--){
      this.years.push(i);
    }
  }
  
  ngOnInit(): void {
    this.createSignUpForm();
  }

  createSignUpForm(){
    this.signUpForm = this.fb.group({
      userName: this.fb.control('', Validators.required),
      userId: this.fb.control('',Validators.required),
      mobile: this.fb.control('',[Validators.required, Validators.min(1000000000),Validators.max(9999999999)]),
      email: this.fb.control('',[Validators.required, Validators.email]),
      emailVerified: this.fb.control(true),
      mobileVerified: this.fb.control(true),
      website:this.fb.control(''),
      profilePic:this.fb.control(this.defaultProfilePic),
      bannerPic:this.fb.control(this.defaultBannerPic),
      bio:this.fb.control(''),
      dob:this.fb.control(''),
      location:this.fb.control(''),
      doj:this.fb.control(this.doj),
      isVerified: this.fb.control(true),
      month: this.fb.control('', Validators.required),
      day: this.fb.control('', Validators.required),
      year: this.fb.control('', Validators.required),
      password: this.fb.control('', [Validators.required, Validators.minLength(6)]),
      confirmPassword: ['', Validators.required],
      followingList: this.fb.array([]),
      followingInt: this.fb.control(0),
      followerList: this.fb.array([]),
      followerInt: this.fb.control(0),
      myLikedList: this.fb.control(0),
      myTweetReplyList: this.fb.control(0),
    }, {
      validator: this.MustMatch('password', 'confirmPassword')
  })
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}

  addUser(){
    console.log(this.signUpForm.value);
    this.registerService.createUser(this.signUpForm.value);
  }

  checkValidation(){
    if(this.signUpForm.controls.userName.valid && this.signUpForm.controls.userId.valid && this.signUpForm.controls.mobile.valid && this.signUpForm.controls.email.valid && this.signUpForm.controls.month.valid && this.signUpForm.controls.day.valid && this.signUpForm.controls.year.valid){
      this.enableExpirence=true;
    }
  }

  updateDay(){
    this.days=[];
    if(this.selectedYear){
      var lastDay = new Date(this.selectedYear, this.selectedMonth + 1, 0);
    }
    else{
      var lastDay = new Date(this.date.getFullYear(), this.selectedMonth + 1, 0);
    }
    for(let i=1;i<=lastDay.getDate();i++){
      this.days.push(i);
    }
  }

  @HostListener('document:click', ['$event'])
  checkClick(event: { target: any; }){
    if(this.name.nativeElement.contains(event.target)){
      this.activeName=true;
    }
    else if(this.phone.nativeElement.contains(event.target)){
      this.activePhone=true;
    }
    else if(this.email.nativeElement.contains(event.target)){
      this.activeEmail=true;
    }
    else{
      this.activeName=false;
      this.activePhone=false;
      this.activeEmail=false;
    }
  }


  showCustomizeSection(){
    this.hide2=false;
    this.hide1=true;
   
  }

  hideInfoSection(){
    this.hide2=true;
    this.hide1=false;
  }

  showPasswordSection(){
    this.hide3=false;
    this.hide2=true;
  }

  hidePasswordSection(){
    this.hide3=true;
    this.hide2=false;
  }

  showSignUpSection(){
    this.hide4=false;
    this.hide3=true;
    let dob:string=this.months[this.selectedMonth]+this.selectedDay+' , '+this.selectedYear;
    this.signUpForm.patchValue({
      userName:this.signUpForm.value.userName,
      mobile:this.signUpForm.value.mobile,
      dob:dob,
    })
  }

  hideSignUpSection(){
    this.hide4=true;
    this.hide3=false;
  }

  closePhone(){
    this.showPhone=false;
    this.showEmail=true;
  }

  closeEmail(){
    this.showPhone=true;
    this.showEmail=false;
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

}
