import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-reply-dialog',
  templateUrl: './reply-dialog.component.html',
  styleUrls: ['./reply-dialog.component.css']
})
export class ReplyDialogComponent implements OnInit {

  txtValue:string = '';
  enable:boolean=false;

  constructor(public dialogRef: MatDialogRef<ReplyDialogComponent>) { }

  ngOnInit(): void {
  }

  onTextChange(value: string)
  {
    this.txtValue = value;
    if(this.txtValue.length)
    {
      this.enable=true;
    }
    else{
      this.enable=false;
    }
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

}
