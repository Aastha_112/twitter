import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-quote-dialog',
  templateUrl: './quote-dialog.component.html',
  styleUrls: ['./quote-dialog.component.css']
})
export class QuoteDialogComponent implements OnInit {

  txtValue:string = '';
  enable:boolean=false;

  constructor(public dialogRef: MatDialogRef<QuoteDialogComponent>) { }

  ngOnInit(): void {
  }

  onTextChange(value: string)
  {
    this.txtValue = value;
    if(this.txtValue.length)
    {
      this.enable=true;
    }
    else{
      this.enable=false;
    }
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

}
