import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { RightSidebarComponent } from './components/right-sidebar/right-sidebar.component';
import { SidebarModule } from 'ng-sidebar';
import { RightPageComponent } from './components/right-page/right-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TweetCardComponent } from '../modules/tweet-card/tweet-card.component';
import { FilterPipe } from '../pipes/filter.pipe';

@NgModule({
  declarations: [
    SidebarComponent,
    RightSidebarComponent,
    RightPageComponent,
    FilterPipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    // NotificationsRoutingModule,
    SidebarModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
  ],
  exports:[
    SidebarComponent,
    RightSidebarComponent,
    RightPageComponent,
  ]
})
export class SharedModule { }
