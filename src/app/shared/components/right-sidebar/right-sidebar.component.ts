import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { USERS } from 'src/app/models/users';
import { RegisterService } from 'src/app/services/register.service';
import { UserDataService } from 'src/app/services/user-data.service';

interface users{
  name:string;
}


@Component({
  selector: 'app-right-sidebar',
  templateUrl: './right-sidebar.component.html',
  styleUrls: ['./right-sidebar.component.css']
})
export class RightSidebarComponent implements OnInit {

  search:FormControl;
  users:any[]=[];
  
  names:string[]=['CricTracker','CricketTracker','CricBuzz']

  constructor(private router:Router,
    public registerService:RegisterService, 
    public userService: UserDataService) { }

  ngOnInit(): void {
    this.registerService.getUser().subscribe(val => {   
      this.users=[];   
        val.map(u=>{
        this.users.push({
          uid:u.payload.doc.id,
          name:u.payload.doc.get('userName'),
          id:u.payload.doc.get('userId'),
          profilePic:u.payload.doc.get('profilePic'),
        })
      })
    });
  }

  searchString:string = '';
  fill:boolean=false;
  status: boolean = false;
  hide:boolean=true;

  @ViewChild('message') message:ElementRef;

  @HostListener('document:click', ['$event'])
  checkClick(event: { target: any; }){
    if(this.message.nativeElement.contains(event.target)){
      this.status=true;
      this.hide=false;
    }
    else{
      this.status=false;
      this.hide=true;
    }
  }

  checkProfile(i){
    let follow = true;
    this.userService.searchUserId(i,follow);
  }

  nullifyValue(){
    this.searchString='';
    this.fill=false;
  }

  onTextChange(value: string)
  {
    this.searchString = value;
    if(this.searchString.length)
    {
      this.fill=true;
      this.hide=false;
    }
    else{
      this.fill=false;
      this.hide=false;
    }
    
  }

  clickEvent(){
    this.status = true;    
  }


}
