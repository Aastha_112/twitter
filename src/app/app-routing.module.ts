import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { from } from 'rxjs';
import { ConversationComponent } from './layouts/conversation/conversation.component';
import { DefaultComponent } from './layouts/default/default.component';
import { FullWidthComponent } from './layouts/full-width/full-width.component';
import { BookmarkPageComponent } from './modules/bookmark-page/bookmark-page.component';
import { ExploreComponent } from './modules/explore/explore.component';
import { HomePageComponent } from './modules/home-page/home-page.component';
import { ListsComponent } from './modules/lists/lists.component';
import { LoginComponent } from './modules/login/login.component';
import { MessagesComponent } from './modules/messages/messages.component';
import { NotificationPageComponent } from './modules/notification-page/notification-page.component';
import { ProfilePageComponent } from './modules/profile-page/profile-page.component';
import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/auth-guard'

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);

const routes: Routes = [
  {
    path:'',
    component:FullWidthComponent,
    children:[
      {
        path:'',
        redirectTo:'login',
        pathMatch:'full',
      },
      {
        path:'login',
        component:LoginComponent,
      }
    ]
  },
  {
    path:'',
    component:DefaultComponent,
    children:[
      {
        path:'home',
        component:HomePageComponent,
        data: {animation: 'HomePage'},
        loadChildren: () => import('./modules/home-page/home-page.module').then(m => m.HomePageModule) 
      },
      {
        path:'explore',
        component:ExploreComponent,
        data: {animation: 'ExplorePage'}
      },
      { 
         path: 'notifications', 
         component:NotificationPageComponent,
         data: {animation: 'NotificationPage'},
         loadChildren: () => import('./modules/notification-page/notification-page.module').then(m => m.NotificationPageModule) 
      },
      { 
        path: 'profile', 
        // component:ProfilePageComponent,
        data: {animation: 'ProfilePage'},
        loadChildren: () => import('./modules/profile-page/profile-page.module').then(m => m.ProfilePageModule) 
     },
      {
        path:'bookmarks',
        component:BookmarkPageComponent,
        data: {animation: 'BookmarkPage'},
        loadChildren: () => import('./modules/bookmark-page/bookmark-page.module').then(m => m.BookmarkPageModule) 
      },
      {
        path:'lists',
        component:ListsComponent,
        data: {animation: 'ListPage'}
      },
    ]
  },
  {
    path:'',
    component:ConversationComponent,
    data: {animation: 'ConversationPage'},
    children:[
      { 
        path: 'messages', 
        component:MessagesComponent,
        loadChildren: () => import('./modules/messages/messages.module').then(m => m.MessagesModule) 
      },
    ]
  },
  
];

@NgModule({
  imports: [RouterModule,RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
