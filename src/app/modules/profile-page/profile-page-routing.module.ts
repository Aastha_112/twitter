import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from '../detail/detail.component';
import { FollowlistComponent } from './followlist/followlist.component';
import { ProfilePageComponent } from './profile-page.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes =[
    {
        path:'',
        component:ProfilePageComponent,
        children:[
            {
                path:':index',
                component:ProfileComponent,
                loadChildren: () => import('../../modules/profile-page/profile/profile.module').then(m => m.ProfileModule) 
            },
            {
                path:':index',
                component:FollowlistComponent,
                loadChildren: () => import('../../modules/profile-page/followlist/followlist.module').then(m => m.FollowlistModule) 
            },
        ]
    },
    
]

@NgModule({
    imports: [RouterModule,RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class ProfilePageRoutingModule { }