import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { FormsModule } from '@angular/forms';
import { TweetsComponent } from './tweets/tweets.component';
import { TweetsRepliesComponent } from './tweets-replies/tweets-replies.component';
import { FollowBtnDirective } from 'src/app/directives/follow-btn.directive';

@NgModule({
  declarations: [
    // ProfileComponent,
    // TweetsComponent,
    // TweetsRepliesComponent,
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    FormsModule,
  ]
})
export class ProfileModule { }
