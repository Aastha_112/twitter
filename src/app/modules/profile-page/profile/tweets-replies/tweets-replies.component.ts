import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SlideInOutAnimation } from 'src/app/animation';

@Component({
  selector: 'app-tweets-replies',
  templateUrl: './tweets-replies.component.html',
  styleUrls: ['./tweets-replies.component.css'],
  animations: [SlideInOutAnimation]
})
export class TweetsRepliesComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  openDetailPage(){
    this.router.navigate(['home/detail']);
  }

}
