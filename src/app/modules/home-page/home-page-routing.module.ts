import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from '../detail/detail.component';
import { HomePageComponent } from './home-page.component';
import { HomeComponent } from './home/home.component';

const routes: Routes =[
    {
        path:'',
        component:HomePageComponent,
        children:[
            {
                path:'',
                component:HomeComponent
            }
        ]
    },
    {
        path:'detail',
        component:DetailComponent
    }
]

@NgModule({
    imports: [RouterModule,RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class HomePageRoutingModule { }