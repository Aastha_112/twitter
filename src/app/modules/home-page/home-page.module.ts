import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { HomePageRoutingModule } from './home-page-routing.module';

@NgModule({
    declarations: [
    //   ProfilePageComponent,
    //   ProfileComponent,
    //   FollowlistComponent
    // HomeComponent
    ],
    imports: [
      // CommonModule,
      RouterModule,
      HomePageRoutingModule,
      FormsModule,
    ]
  })
  export class HomePageModule { }