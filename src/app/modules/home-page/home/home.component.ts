import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { SlideInOutAnimation } from 'src/app/animation';
import { RegisterService } from 'src/app/services/register.service';
import { UserDataService } from 'src/app/services/user-data.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { TweetService } from 'src/app/services/tweet.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [SlideInOutAnimation]
})
export class HomeComponent implements OnInit {

  show:boolean=false;
  txtValue:string = '';
  enable:boolean=false;
  isTweet:boolean=true;
  user:any[]=[];
  tweets:any[]=[];
  description:string;
  tweetForm:FormGroup;
  userName:string[]=[];
  userId:string[]=[];
  defaultPic:string[]=[];
  profilePic:string;
  currentUser:string;
  imageUrl:string='';
  selectedImg;
  selectedFile: File ;
  fbs;
  downloadURL: Observable<string>;

  constructor(public registerService:RegisterService, 
    public userService: UserDataService,
    public fireStore: AngularFirestore,
    public tweetService: TweetService,
    private storage: AngularFireStorage,
    public fb: FormBuilder,) { }

  ngOnInit(): void {
    
    this.tweetService.getTweet().subscribe(val => {
      this.tweets=[];
      val.map(u=>{
        if(u.payload.doc.get('UID') == this.userService.getUserId()){
          this.tweets.push({
            description:u.payload.doc.get('description'),
            name:this.userName,
            id:this.userId,
            likes:u.payload.doc.get('likeCount'),
            comment:u.payload.doc.get('commentCount'),
            retweet:u.payload.doc.get('retweetCount'),
            profilePic:u.payload.doc.get('profilePic'),
          })
        }
      })
    });
    this.registerService.getUser().subscribe(val => {
      val.map(u=>{
        if(u.payload.doc.id == this.userService.getUserId()){
          this.userName.push(u.payload.doc.get('userName'));
          this.userId.push(u.payload.doc.get('userId'));
          this.defaultPic.push(u.payload.doc.get('profilePic'));
          this.profilePic=u.payload.doc.get('profilePic')
        }
      })
    });
    this.createTweet();
  }

  createTweet(){    
    console.log(this.userService.getUserId());
    
    this.tweetForm = this.fb.group({
      UID:this.fb.control(this.userService.getUserId(),Validators.required),
      description:this.fb.control('',[Validators.required, Validators.maxLength(280)]),
      userName:this.fb.control(this.userName,Validators.required),
      userId:this.fb.control(this.userId,Validators.required),
      profilePic:this.fb.control(this.defaultPic),
      likedByuserList:this.fb.array([]),
      likeCount:this.fb.control(0),
      reTweetByuserList:this.fb.array([]),
      commentCount:this.fb.control(0),
      retweetCount:this.fb.control(0),
      createdAt:this.fb.control(Date.now(),Validators.required),
      location:this.fb.control('Ahmedabad'),
      media:this.fb.array([]),
      isDeleted:this.fb.control(false),
      isPinned:this.fb.control(false),
    })
  }

  addTweet(){
    if(this.enable){
      this.tweetService.createTweet(this.tweetForm.value);
      this.tweetForm.reset();
    }
    this.enable=false;
    this.imageUrl='';
  }


  onTextChange(value: string)
  {
    this.txtValue = value;
    if(this.txtValue.length)
    {
      this.enable=true;
    }
    else{
      this.enable=false;
    }
  }



}
function finalize(arg0: () => void): import("rxjs").OperatorFunction<import("firebase").default.storage.UploadTaskSnapshot | undefined, unknown> {
  throw new Error('Function not implemented.');
}

