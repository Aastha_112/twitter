import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SlideInOutAnimation } from 'src/app/animation';
import { Location } from '@angular/common';
import { ReplyDialogComponent } from 'src/app/dialog-box/reply-dialog/reply-dialog.component';
import { QuoteDialogComponent } from 'src/app/dialog-box/quote-dialog/quote-dialog.component';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
  animations:[SlideInOutAnimation]
})
export class DetailComponent implements OnInit {

  showChildReply:boolean=false;
  liked:boolean=false;
  retweeted:boolean=false;
  showRetweetOption:boolean=false;
  likeCount:number=834;
  retweetCount:number=7;
  show:boolean=false;
  reply:boolean=true;
  parent:boolean=true;
  child:boolean=true;

  @ViewChild('retweet') retweet:ElementRef;
  @ViewChild('share') share:ElementRef;

  constructor(public replyDialog:MatDialog, private location: Location, public quoteDialog:MatDialog) { }

  ngOnInit(): void {
  }

  goBack() {
    this.location.back();
  }

  openDialog(){    
    this.replyDialog.open(ReplyDialogComponent);
  }

  openQuoteDialog(){    
    this.quoteDialog.open(QuoteDialogComponent);
  }

  @HostListener('document:click', ['$event'])
  checkClick(event: { target: any; }){
    if(this.retweet.nativeElement.contains(event.target)){
      this.showRetweetOption=true;
    }
    else if(this.share.nativeElement.contains(event.target)){
      this.show=true;
    }
    else{
      this.showRetweetOption=false;
      this.show=false;
    }
  }

  childReply(){
    this.showChildReply=true;
  }

  hideBox(){
    this.show=false;
  }

  showRetweeted(){
    this.showRetweetOption=true;
  }

  showRetweetBox(){
    this.showRetweetOption=true;    
  }
  showRetweetOption1:boolean=false
  showRetweetBox1(){
    this.showRetweetOption1=true;
    console.log(this.showRetweetOption1);
  }

  hideRetweetBox(){
    this.showRetweetOption=false;
    this.showRetweetOption1=false;
    console.log(this.showRetweetOption1);
  }
  increaseLike(){
    if(this.liked)
    {
      this.likeCount--;
    }
    else{
      this.likeCount++;
    }
    this.liked=!this.liked;
  }

  increaseLikes(){
    if(this.liked){
      this.likeCount--;
    }
    else{
      this.likeCount++;
    }
    this.liked=!this.liked;
  }

  increaseRetweet(){
    this.retweetCount++;
    this.retweeted=true;
  }

  decreaseRetweet(){
    this.retweetCount--;
    this.retweeted=false;
  }

}
