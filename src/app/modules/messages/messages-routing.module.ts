import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComposeComponent } from './compose/compose.component';
import { MessagesComponent } from './messages.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
  {
    path:'settings',
    component:SettingsComponent
  },
  // {
  //   path:'compose',
  //   component:ComposeComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessagesRoutingModule { }
