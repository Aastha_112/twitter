import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NotificationsComponent } from './notifications/notifications.component';
import { NotificationPageRoutingModule } from './notification-page-routing.module';

@NgModule({
    declarations: [
      NotificationsComponent,
    ],
    imports: [
      // CommonModule,
      RouterModule,
      NotificationPageRoutingModule,
      FormsModule,
    ]
  })
  export class NotificationPageModule { }