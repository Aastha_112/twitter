import { Component, OnInit } from '@angular/core';
import { UserDataService } from './services/user-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'twitter';

  constructor(public userService: UserDataService){}

  ngOnInit(){
    // let uid = this.userService.getUserId();
    // console.log(uid);
  }
}
