import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  currentUser:string;
  searchUser:string;
  follow:boolean;
  profile:boolean;
  followingList:string[];

  constructor() { }

  followinglist(list){
    this.followingList.push(list);
  }

  getFollowingList(){
    return this.followingList;
  }

  getCurrentUser(id){
    this.currentUser=id;
  }

  getUserId(){
    return this.currentUser;
  }

  searchUserId(userId,follow){
    console.log('search');
    this.searchUser=userId;
    this.follow=follow;
  }

  getSearchUserId(){
    return this.searchUser;
  }
  showProfilePage(){
    return true;
  }
}
